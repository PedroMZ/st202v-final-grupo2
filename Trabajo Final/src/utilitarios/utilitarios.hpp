/*
 * utilitarios.hpp
 *
 *  Created on: 3 jun. 2018
 *      Author: Zambrano
 */

#ifndef UTILITARIOS_UTILITARIOS_HPP_
#define UTILITARIOS_UTILITARIOS_HPP_
#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
unsigned long long stringaunsillong(string texto);
int stringaint(string texto);
string intastring(int n);
string floatastring(float n);
float stringafloat(string texto);
struct repartidor obtenerRepartidor(string datoRepartidor);
unsigned long long Karatsuba (unsigned long long &n1, unsigned long long &n2);
bool buscarDatosRepetidosComp(string dato);
string unsignedastring(unsigned long long n);
void imprimirCategoriasProductos();
vector <string> obtenerProductos(int categoria);
void imprimirProductos(vector <string> productos);
struct comprador obtenerComprador(string telefono);
bool buscarPalabraTienda(string dato);
vector <string> separarDatos(string linea);
unsigned long long contra(unsigned long long n);
float buscarPrecio(string producto,string usuarioTienda);
vector <struct pedidoHistorial> obtenerPedidosPendientes(struct comprador comprador);
vector <struct pedidoHistorial> obtenerVectorEstructuraPedidos(vector <string> vectorArchivoHistorialPedidos);
vector <string> obtenerVectorArchivo(string rutaArchivo);
struct tienda obtenerTienda(string usuario);
vector < pair <struct tienda,float> > buscarTiendas(string producto,int coordx,int coordy);
void generarPedido(string producto,int cantidad,float precioTotal,float pagaCon,bool casa,int coordx,int coordy,struct tienda tienda,struct comprador comprador);
void agregarHistorial(string producto,int cantidad,float precioTotal,struct tienda tienda,struct comprador comprador);
vector <struct pedidoHistorial> obtenerFacturas(struct comprador comprador);
void imprimirFacturas(vector <struct pedidoHistorial> pedidos);
void cancelarPedido(string telefonoComprador,string horaPedido);
vector <struct pedidoHistorial> obtenerPedidosFinalizados(struct comprador comprador);
void imprimirPedidosFinalizados(vector <struct pedidoHistorial> pedidos);
void modificarRepartidor(struct repartidor repartidor);
void cambiarEstadoPuntuacionHistorialPedidos(struct pedidoHistorial pedido);
struct comprador{
	string nombre;
	unsigned long long numero;
	string correo;
	string direccion;
	int coordX;
	int coordY;
	string preguntaSecreta;
	string respuestaSecreta;
	unsigned long long contrasenia;
	void inicializarDatos(vector<string> datos){
		nombre = datos[0];
		numero = stringaunsillong(datos[1]);
		correo = datos[2];
		direccion = datos[3];
		coordX = stringaint(datos[4]);
		coordY = stringaint(datos[5]);
		preguntaSecreta=datos[6];
		respuestaSecreta=datos[7];
		contrasenia=stringaint(datos[8]);
	}
	string crearLinea(){
		string linea;
		return linea=nombre+'|'+unsignedastring(numero)+'|'+correo+'|'+direccion+'|'+intastring(coordX)+'|'+intastring(coordY)+'|'+preguntaSecreta+'|'+respuestaSecreta+'|'+unsignedastring(contrasenia);
	}

		void agregarComprador(){
			string linea=crearLinea();
			string rutaArchivo="archivos/compradores/listaCompradores.txt";
			ofstream archivo;
			archivo.open(rutaArchivo.c_str(),ios::app);
			if(archivo.is_open()){
				archivo << linea << endl;
			}else{
				cout << "El archivo no pudo abrirse" << endl;
			}
			archivo.close();
		}


		void crearArchivoConfirmacion(){
			string rutaArchivo="archivos/compradores/confirmacion/"+correo+".txt";
			ofstream archivo;
			archivo.open(rutaArchivo.c_str(),ios::trunc);
			if(archivo.is_open()){
				archivo << "Sus datos son los siguientes:\n" << endl;
				archivo << "Nombre: " << nombre << endl;
				archivo << "Numero de telefono: " << numero << endl;
				archivo << "Correo electronico: " << correo << endl;
				archivo << "Direccion: " << direccion << endl;
				archivo << "Coordenadas XY (separadas por espacio): " << coordX << " " << coordY << endl;
				archivo << "Pregunta secreta: " << preguntaSecreta << endl;
				archivo << "Respuesta secreta: " << respuestaSecreta << endl;
				archivo << "Contrasenia: " << contrasenia;
			}else{
				cout << "El archivo no pudo abrirse" << endl;
			}
			archivo.close();
		}
};
bool buscarTienda(string dato);
bool buscarRepartidor(string dato);
struct pedidoHistorial{
	string nombreProducto;
	int cantidad;
	float montoTotal;
	string nombreTienda;
	string telefonoComprador;
	string fechaPedido;
	string horaPedido;
	string fechaEntrega;
	string horaEntrega;
	string repartidor;
	string telefonoRepartidor;
	string estadoPuntuacion;
	string estadoPedido;

	void inicializarDatos(vector <string> datos){
		nombreProducto=datos[0];
		cantidad=stringaint(datos[1]);
		montoTotal=stringafloat(datos[2]);
		nombreTienda=datos[3];
		telefonoComprador=datos[4];
		fechaPedido=datos[5];
		horaPedido=datos[6];
		fechaEntrega=datos[7];
		horaEntrega=datos[8];
		repartidor=datos[9];
		telefonoRepartidor=datos[10];
		estadoPuntuacion=datos[11];
		estadoPedido=datos[12];
	}

	string crearLinea(){
		string linea;
		return linea=nombreProducto+'|'+intastring(cantidad)+'|'+floatastring(montoTotal)+'|'+nombreTienda+'|'+telefonoComprador+'|'+fechaPedido+'|'+horaPedido+'|'+fechaEntrega+'|'+horaEntrega+'|'+repartidor+'|'+telefonoRepartidor+'|'+estadoPuntuacion+'|'+estadoPedido;
	}
};


//tiendas
struct tienda{
	string nombre;
	unsigned long long numero;
	string usuario;
	string correo;
	string direccion;
	int coordX;
	int coordY;
	unsigned long long contrasenia;

	void inicializarConVector(vector<string> datos){
		nombre = datos[0];
		numero = stringaunsillong(datos[1]);
		usuario = datos[2];
		correo = datos[3];
		direccion = datos[4];
		coordX = stringaint(datos[5]);
		coordY = stringaint(datos[6]);
		contrasenia=stringaint(datos[7]);
	}

	string crearLinea(){
		string linea;
		return linea=nombre+'|'+intastring(numero)+'|'+usuario+'|'+correo+'|'+direccion+'|'+intastring(coordX)+'|'+intastring(coordY)+'|'+intastring(contrasenia);
	}

	void ingresarDatos(int i,string dato){
		switch (i){
		case 0: nombre=dato;
		break;
		case 1: numero=stringaunsillong(dato);
		break;
		case 2: usuario=dato;
		break;
		case 3: correo=dato;
		break;
		case 4: direccion=dato;
		break;
		case 5:	coordX=stringaint(dato);
		break;
		case 6: coordY=stringaint(dato);
		break;
		case 7: contrasenia=stringaunsillong(dato);
		break;
		}
	}

	void crearContrasenia(){
		unsigned long long valor=numero%1000;
		contrasenia=contra(valor);
	}

	void agregarTienda(){
		string linea=crearLinea();

		string rutaArchivo="archivos/tiendas/listaTiendas.txt";
		ofstream archivo;
		archivo.open(rutaArchivo.c_str(),ios::app);
		if(archivo.is_open()){
			archivo << linea << endl;
		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();
	}

	void crearArchivoConfirmacion(){

		string rutaArchivo="archivos/tiendas/confirmacion/"+correo+".txt";
		ofstream archivo;
		archivo.open(rutaArchivo.c_str(),ios::trunc);
		if(archivo.is_open()){
			archivo << "Sus datos son los siguientes:\n" << endl;
			archivo << "Nombre de la tienda: " << nombre << endl;
			archivo << "Numero de telefono: " << numero << endl;
			archivo << "Usuario: " << usuario << endl;
			archivo << "Correo electronico de contacto: " << correo << endl;
			archivo << "Direccion de la tienda: " << direccion << endl;
			archivo << "Coordenadas XY (separadas por espacio): " << coordX << " " << coordY << endl;
			archivo << "Contrasenia: " << contrasenia;
		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();
	}
};

struct producto{
	string categoria;
	string tienda;
	string nombre;
	float precio;

	void inicializar(string opcion,string usuario,string nombreProducto){
		if(opcion=="1"){
			categoria="Viveres";
		}else if(opcion=="2"){
			categoria="Confiteria";
		}else if(opcion=="3"){
			categoria="Comida Rapida";
		}
		tienda=usuario;
		nombre=nombreProducto;
	}

	string crearLinea(){
		string linea;
		return linea=categoria+'|'+tienda+'|'+nombre+'|'+floatastring(precio);
	}

	void agregarProducto(){
		string linea=crearLinea();
		string rutaArchivo="archivos/productos/listaTiendayProductos.txt";
		ofstream archivo;
		archivo.open(rutaArchivo.c_str(),ios::app);
		if(archivo.is_open()){
			archivo << linea << endl;
		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();
	}
};


//fin tiendas


//repartidor
struct repartidor{
	string nombre;
	unsigned long long numero;
	string DNI;
	string usuario;
	string correo;
	unsigned long long password;
	float puntuacion;
	int numpuntuaciones;
	vector <struct pedidoDisponible> obtenerPedidosDisponibles();

	void inicializarVector(vector<string> datos){
		nombre = datos[0];
		numero = stringaunsillong(datos[1]);
		DNI = datos[2];
		usuario = datos[3];
		correo = datos[4];
		password = stringaunsillong(datos[5]);
		puntuacion=stringafloat(datos[6]);
		numpuntuaciones=stringaint(datos[7]);
	}

	void ingresarPuntuacion(float punt){
		float total;
		total = puntuacion*numpuntuaciones;
		numpuntuaciones++;
		puntuacion=(total+punt)/numpuntuaciones;
	}

	void crearClave(){
		unsigned long long valor=numero%10000;
		password=contra(valor);
	}

	string crearLinea(){
		string linea;
		return linea=nombre+'|'+intastring(numero)+'|'+DNI+'|'+usuario+'|'+correo+'|'+ intastring(password)+'|'+floatastring(puntuacion)+'|'+intastring(numpuntuaciones);
	}

	void agregarRepartidor(){
		string linea=crearLinea();
		string rutaArchivo="archivos/repartidores/listaRepartidores.txt";
		ofstream archivo;
		archivo.open(rutaArchivo.c_str(),ios::app);
		if(archivo.is_open()){
			archivo << linea << endl;
		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();
	}

	void crearArchivoConfirmacion(){
		string rutaArchivo="archivos/repartidores/confirmacion/"+correo+".txt";
		ofstream archivo;
		archivo.open(rutaArchivo.c_str(),ios::trunc);
		if(archivo.is_open()){
			archivo << "Sus datos son los siguientes:\n" << endl;
			archivo << "Nombre del repartidor: " << nombre << endl;
			archivo << "Numero de telefono: " << numero << endl;
			archivo << "DNI: " << DNI << endl;
			archivo << "Usuario: " << usuario << endl;
			archivo << "Contrasenia: " << password<<endl;
			archivo << "Puntacion: "<< puntuacion<<endl;
			archivo << "Numero de puntuaciones: "<< numpuntuaciones<<endl;

		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();
	}
};
struct pedidoDisponible{
	string nombreProducto;
	string horaPedido;
	string fechaPedido;
	string nombreTienda;
	string direccionTienda;
	int cantidad;
	float montoTotal;
	float pagaCon;
	string nombreComprador;
	string direccionComprador;
	int coordX;
	int coordY;
	string telefonoComprador;

	void inicializarVector(vector <string> datos){
		nombreProducto=datos[0];
		horaPedido=datos[1];
		fechaPedido=datos[2];
		nombreTienda=datos[3];
		direccionTienda=datos[4];
		cantidad=stringaint(datos[5]);
		montoTotal=stringafloat(datos[6]);
		pagaCon=stringafloat(datos[7]);
		nombreComprador=datos[8];
		direccionComprador=datos[9];
		coordX=stringaint(datos[10]);
		coordY=stringaint(datos[11]);
		telefonoComprador=datos[12];
	}

	string crearLinea(){
		string linea;
		return linea=nombreProducto+'|'+horaPedido+'|'+fechaPedido+'|'+nombreTienda+'|'+direccionTienda+'|'+intastring(cantidad)+'|'+floatastring(montoTotal)+'|'+floatastring(pagaCon)+'|'+nombreComprador+'|'+direccionComprador+'|'+intastring(coordX)+'|'+intastring(coordY)+'|'+telefonoComprador;
	}
};
string obtenerFecha();
string obtenerHora();

void FinalizarPedidoHistorial(string telefonoComprador,string horaPedido);
vector <struct pedidoDisponible> obtenerPedidosDisponibles();

void imprimirPedidosDisponibles(vector <struct pedidoDisponible> pedidosDisponibles);

void modificarHistorial(string telefonoComprador,string horaPedido,struct repartidor repartidor);
vector <struct pedidoHistorial> obtenerPedidosTomados(struct repartidor repartidor);
void imprimirPedidosTomados(vector <struct pedidoHistorial> pedidosTomados);
//fin repartidores

void cerrarApp();
#endif /* UTILITARIOS_UTILITARIOS_HPP_ */
