/*
 * principal.cpp
 *
 *  Created on: 6 jun. 2018
 *      Author: Zambrano
 */

#include "../utilitarios/utilitarios.hpp"
#include "../compradores/compradores.hpp"
#include "../tiendas/tiendas.hpp"
#include "../repartidores/repartidores.hpp"

void pantallaPrincipal(){
	cout << "\n\n\n\n";
		cout << "\t\t\tBienvenido a Delivery Kwik-E-Mart Apu's"<<endl;
		Sleep(2000);
		int m;
		cout<<"\n�Que tipo de usuario es?"<<endl;
		cout<<"1. Comprador"<<endl;
		cout<<"2. Repartidor"<<endl;
		cout<<"3. Tienda"<<endl;
		cout<<"0. Salir"<<endl;
		cin>>m;
		switch(m){
		case 1:
			interfazComprador();
			break;
		case 2:
			pantallaRepartidor();
			break;
		case 3:
			Tiendas();
			break;
		case 0:
			cerrarApp();
			break;
		default:
			cout<<"Por favor, ingrese una opcion valida"<<endl;
		}
	}


