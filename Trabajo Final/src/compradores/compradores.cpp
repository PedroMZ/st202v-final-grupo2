	//============================================================================
// Name        : comprador.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include "../utilitarios/utilitarios.hpp"
#include "../principal/principal.hpp"
//Imprimir datos comprador
void imprimirDatosConfirmacionComp(struct comprador A){
	cout<<"Nombre: "<<A.nombre<<endl;
	cout<<"Telef: "<<A.numero<<endl;
	cout<<"E-mail: "<<A.correo<<endl;
	cout<<"Direcci�n: "<<A.direccion<<endl;
	cout<<"Coordenadas: "<<A.coordX<<","<<A.coordY<<endl;
	cout<<"Pregunta Secreta: "<<A.preguntaSecreta<<endl;
	cout<<"Respuesta: "<<A.respuestaSecreta;
}
//Aqui termina

void compradorLog(string telefono);
void estadoPedido(struct comprador comprador);
void compradorComprar(struct comprador comprador);
void verPedidosFinalizados(struct comprador comprador);

//Inicia Imprimir Pedidos Pendientes
void imprimirPedidosPendientes(vector <struct pedidoHistorial> pedidos){
	if(pedidos.size()==0){
		cout << "\nNo tiene ningun pedido pendiente" << endl;
	}else{
		cout << "\nPedidos pendientes:\n" << endl;
		for(unsigned int i=0;i<pedidos.size();i++){
			if(pedidos[i].estadoPedido=="disponible"){
				cout << "--> Pedido numero " << (i+1) << ":" << endl;
				cout << "* Nombre de producto: " << pedidos[i].nombreProducto << endl;
				cout << "* Cantidad: " << pedidos[i].cantidad << endl;
				cout << "* Monto a pagar: S/." << pedidos[i].montoTotal << endl;
				cout << "* Nombre de la tienda: " << pedidos[i].nombreTienda << endl;
				cout << "* Fecha del pedido: " << pedidos[i].fechaPedido << endl;
				cout << "* Hora del pedido: " << pedidos[i].horaPedido << endl;
				cout << endl;
			}else if(pedidos[i].estadoPedido=="tomado"){
				cout << "--> Pedido numero " << (i+1) << ":" << endl;
				cout << "* Nombre de producto: " << pedidos[i].nombreProducto << endl;
				cout << "* Cantidad: " << pedidos[i].cantidad << endl;
				cout << "* Monto a pagar: S/." << pedidos[i].montoTotal << endl;
				cout << "* Nombre de la tienda: " << pedidos[i].nombreTienda << endl;
				cout << "* Fecha del pedido: " << pedidos[i].fechaPedido << endl;
				cout << "* Hora del pedido: " << pedidos[i].horaPedido << endl;
				cout << "* Nombre del repartidor: " << pedidos[i].repartidor << endl;
				cout << "* Telefono del repartidor: " << pedidos[i].telefonoRepartidor << endl;
				cout << endl;
			}
		}
	}
}
//Acaba Imprimir Pedidos Pendientes





//Inicia Ver Pedidos Pendientes
void verPedidosPendientes(struct comprador comprador){
	system("cls");
	string opcion;
	unsigned int opcionPedido;
	vector <struct pedidoHistorial> pedidosPendientes=obtenerPedidosPendientes(comprador);
	imprimirPedidosPendientes(pedidosPendientes);
	cout << "Opciones disponibles: " << endl;
	cout << "1. Cancelar un pedido" << endl;
	cout << "0. Regresar" << endl;
	cout << "\nPor favor elija una opcion: ";
	cin >> opcion;
	if(opcion=="1"){
		cout << "\nPor favor elija el pedido que quiere cancelar: ";
		cin >> opcionPedido;
		if(opcionPedido<=pedidosPendientes.size() && pedidosPendientes[opcionPedido-1].estadoPedido=="disponible"){
			cout << "\nEsta seguro que desea cancelar el pedido seleccionado? (S/N): ";
			cin >> opcion;
			if(opcion=="s" || opcion=="S"){
				cancelarPedido(pedidosPendientes[opcionPedido-1].telefonoComprador,pedidosPendientes[opcionPedido-1].horaPedido);
				cout << "\nPedido borrado con exito" << endl;
				Sleep(2000);
				compradorLog(intastring(comprador.numero));
			}else if(opcion=="n" || opcion=="N"){
				estadoPedido(comprador);
			}else{
				while(true){
					cout << "\nPor favor elija una opcion valida" << endl;
					cin >> opcion;
					if(opcion=="s" || opcion=="S"){
						cancelarPedido(pedidosPendientes[opcionPedido-1].telefonoComprador,pedidosPendientes[opcionPedido-1].horaPedido);
						cout << "\nPedido borrado con exito" << endl;
						Sleep(2000);
						compradorLog(intastring(comprador.numero));
					}else if(opcion=="n" || opcion=="N"){
						estadoPedido(comprador);
					}
				}
			}
		}else{
			cout << "Por favor elija una opcion valida" << endl;
			Sleep(2000);
			verPedidosPendientes(comprador);
		}
	}else if(opcion=="0"){
		estadoPedido(comprador);
	}else{
		cout << "\nPor favor elija una opcion valida" << endl;
		Sleep(2000);
		verPedidosPendientes(comprador);
	}
}
//Fin de Ver Pedidos Pendientes








//Incia Estado Pedido
void estadoPedido(struct comprador comprador){
	system("cls");
	int opcion;
	cout<<"\nBienvenido "<<comprador.nombre<<endl;
	cout<<"\nQue desea hacer?"<<endl;
	cout<<"1. Ver pedidos pendientes"<<endl;
	cout<<"2. Puntuar un repartidor"<<endl;
	cout<<"0. Regresar"<<endl;
	cout<<"\nPor favor elija una opcion: ";
	cin >> opcion;
	switch(opcion){
		case 1:
			verPedidosPendientes(comprador);
			break;
		case 2:
			verPedidosFinalizados(comprador);
			break;
		case 0:
			compradorLog(unsignedastring(comprador.numero));
			break;
		default:
			cout<<"\nPor favor, elija una opcion valida..."<<endl;
			Sleep(2000);
			estadoPedido(comprador);
	}

}



//Fin Estado Pedido


//Inicia Ver Facturas
void verFacturas(struct comprador comprador){
	system("cls");
	string opcion;
	vector <struct pedidoHistorial> facturas=obtenerFacturas(comprador);
	imprimirFacturas(facturas);
	cout<<"\nPresione 0 para volver: ";
	cin>>opcion;
	if(opcion=="0"){
		compradorLog(intastring(comprador.numero));
	}else{
		while(true){
			cout<<"\nPor favor ingrese una opcion valida:";
			cin>>opcion;
			if(opcion=="0"){
				compradorLog(intastring(comprador.numero));
			}
		}
	}

}
//Fin Ver Facturas

//incia comprador logeado
void compradorLog(string telefono){
	system("cls");
	int opcion;
	struct comprador comprador=obtenerComprador(telefono);
	cout<<"\nBienvenido "<<comprador.nombre<<endl;
	cout<<"\nOpciones disponibles"<<endl;
	cout<<"1. Ver estado de pedido"<<endl;
	cout<<"2. Comprar"<<endl;
	cout<<"3. Ver facturas"<<endl;
	cout<<"0. Salir"<<endl;
	cout<<"\nPor favor, elija una opcion: ";
	cin>>opcion;
	switch(opcion){
		case 1:
			estadoPedido(comprador);
			break;
		case 2:
			compradorComprar(comprador);
			break;
		case 3:
			verFacturas(comprador);
			break;
		case 0:
			cerrarApp();
			break;
		default:
			cout<<"\nPor favor, elija una opcion valida"<<endl;
			Sleep(2000);
			compradorLog(telefono);
	}



}
//fin comprador logeado



//Inicia ingresa comprador
void ingresarComprador(){
	system("cls");
	string telefono, archivoCompradores, linea, nombre, password, passOK, dato, pregunta, respuesta, respuestaOK;
	cout<<"\n     Bienvenido a la interfaz de identificacion de compradores"<<endl;
	cout<<"\nPor favor, digite su numero telefonico: "; cin>>telefono;
	archivoCompradores="archivos/compradores/listaCompradores.txt";
	ifstream archivo;
	archivo.open(archivoCompradores.c_str());
	if(archivo.is_open()){
		bool hallado=false;
		while(!archivo.eof() && hallado==false){
			getline(archivo,linea);
			if(linea!=""){
				if(linea.find(telefono)<100){
					stringstream ss(linea);
					getline(ss,nombre,'|');
					hallado=true;

				}
			}
		}
		archivo.close();
		if(hallado==true){
			cout<<"\nBienvenido "<<nombre<<endl;
			cout<<"\nPor favor, ingrese su password :";
			cin>>password;
			int  i=0;
			char separador;
			stringstream ss(linea);
			while(i<9){
				if(i==8){
					separador='\n';
				}else{
					separador='|';
				}
				getline(ss,passOK,separador);
				i++;
			}
			i=0;
			while(password!=passOK && i<3){
				if(i==2){
					cout<<"\nPassword incorrecta, le queda 1 intento...";
				}else{
					cout<<"\nPassword incorrecta, le quedan "<<3-i<<" intentos...";
				}
			cin>>password;
			i++;
			}
			if(password!=passOK && i==3){
				stringstream ss(linea);
				int j=0;
				while(j<9){
					if(j==8){
						separador='\n';
					}else{
						separador='|';
					}
					getline(ss,dato,separador);
					if(j==1){
						telefono=dato;
					}else if(j==6){
						pregunta=dato;
					}else if(j==7){
						respuestaOK=dato;
					}
					j++;
				}
				cout<<"\nFallo tres veces al ingresar el password... Por favor, responda su pregunta secreta: "<<endl;
				cout<<pregunta<<endl;
				cin.ignore();
				getline(cin,respuesta);
				if(respuesta!=respuestaOK){
					cout<<"\nLa respuesta a la pregunta secreta es incorrecta...Por favor, intente nuevamente ";
					Sleep(3000);
					ingresarComprador();
				}else{
					compradorLog(telefono);
				}
			}else{
				cout<<"\nBienvenido "<<nombre<<endl;
				Sleep(3000);
				compradorLog(telefono);
			}
		}else{
			cout<<"\nEl numero de telefono no esta registrado... Por favor, intente nuevamente ";
			Sleep(3000);
			ingresarComprador();
		}
	}else{
		cout<<"No se pudo abrir el archivo..."<<endl;
	}
}
//fin ingresar comprador





//registrar comprador
void registrarComprador(){
	struct comprador C1;
	system("cls");
	cout<<"\n             Bienvenido a la interfaz de Registro de Compradores"<<endl;
	cout<<"              Gracias por querer ser parte de nuestros clientes"<<endl;
	cout<<"\nPara empezar necesitamos que por favor digite el nombre como le gustar�a q nos dirijiesemos a usted: "<<endl;
	cin.ignore();
	getline(cin,C1.nombre);
	cout<<"\nMucho gusto "<<C1.nombre<<", ahora por favor digite su numero de telefono: "<<endl;
	cin>>C1.numero;
	bool repoio=buscarDatosRepetidosComp(unsignedastring(C1.numero));
	if(repoio==true){
		do{
			cout<<"\nLo sentimos, el numero ingresado ya se encuentra registrado"<<endl;
			cout<<"\nPor favor, digite su numero de telefono: "<<endl;
			cin>>C1.numero;
			repoio=buscarDatosRepetidosComp(unsignedastring(C1.numero));
		}while(repoio==true);
	}
	cout<<"Muy bien, ahora digite por favor su correo electronico"<<endl;
	cin>>C1.correo;
	repoio=buscarDatosRepetidosComp(C1.correo);
	if(repoio==true){
		do{
			cout<<"\nLo sentimos, el correo ingresado ya se encuentra registrado"<<endl;
			cout<<"\nPor favor, digite su correo: "<<endl;
			cin>>C1.correo;
			repoio=buscarDatosRepetidosComp(C1.correo);
		}while(repoio==true);
	}
	cout<<"\nAhora necesitamos que digite su direccion por favor: ";
	cin.ignore();
	getline(cin,C1.direccion);
	cout<<"\nPor favor, necesitamos que usted digite las coordenadas de su casa (Puede encontrarlas en google Maps)"<<endl;
	cin>> C1.coordX >> C1.coordY;
	cout<<"\nEn caso olvide su contrasenia, necesitamos que digite una pregunta q solo usted pueda responder (Use unicamente el signo'?' al final de la pregunta)"<<endl ;
	cin.ignore();
	getline(cin,C1.preguntaSecreta);
	cout<<"Muy bien, para finalizar ingrese por favor la respuesta a esa pregunta secreta"<<endl;
	getline(cin,C1.respuestaSecreta);
	unsigned long long n1;
	unsigned long long n2;
	n1=C1.numero/5;
	n2=C1.numero%5;
	C1.contrasenia=Karatsuba(n1,n2);
	cout<<"\nSus datos son:"<<endl;
	cout<<endl;
	imprimirDatosConfirmacionComp(C1);
	char conf1;
	cout<<"\nSus datos son correctos? (S/N)"<<endl;cin>>conf1;
	switch(conf1){
		case 'S':
			C1.agregarComprador();
			C1.crearArchivoConfirmacion();
			cout << "\nComprador registrado con exito" << endl;
			cout << "Recibira un correo de confirmacion";
			Sleep(5000);
			exit(0);
			break;
		case 's':
			C1.agregarComprador();
			C1.crearArchivoConfirmacion();
			cout << "\nComprador registrado con exito" << endl;
			cout << "Recibira un correo de confirmacion";
			Sleep(5000);
			exit(0);
			break;
		case 'N':
			registrarComprador();
			break;
		case 'n':
			registrarComprador();
			break;
		default:
			cin>>conf1;
			do{
				switch(conf1){
						case 'S':
							C1.agregarComprador();
							C1.crearArchivoConfirmacion();
							cout << "\nComprador registrado con exito" << endl;
							cout << "Recibira un correo de confirmacion";
							Sleep(5000);
							exit(0);
							break;
						case 's':
							C1.agregarComprador();
							C1.crearArchivoConfirmacion();
							cout << "\nComprador registrado con exito" << endl;
							cout << "Recibira un correo de confirmacion";
							Sleep(5000);
							exit(0);
							break;
						case 'N':
							registrarComprador();
							break;
						case 'n':
							registrarComprador();
							break;
				}
			}while(conf1=='S'||conf1=='s'||conf1=='N'||conf1=='n');
	}
}
//Aqui termina registrar comprador

//Interfaz comprador
void interfazComprador(){
	system("cls");
	cout<<"     Bienvenido a la interfaz de Compradores del Delivery Kwik-E-Mart Apu's"<<endl;
	cout<<"\nEstas son las opciones que puede realizar"<<endl;
	cout<<"\n1. Ingresar"<<endl;
	cout<<"2. Registrarse"<<endl;
	cout<<"3. Regresar"<<endl;
	cout<<"0. Salir"<<endl;
	int n;
	cin>>n;
	switch(n){
		case 1:
			ingresarComprador();
			break;
		case 2:
			registrarComprador();
			break;
		case 3:
			pantallaPrincipal();
			break;
		case 0:
			cerrarApp();
			break;
		default:
			cout<<"\nPor favor, ingrese una de las opciones mostradas"<<endl;
			Sleep (3000);
			interfazComprador();
	}
}
//aqui termina interfaz comprador

//CompradorComprar
void compradorComprar(struct comprador comprador){
	system("cls");
	int opcion;
	string opcion2;
	int opcionDireccion;
	int coordX;
	int coordY;
	int cantidad;
	bool casa;
	bool bueningreso;
	cout<<"\nPor favor, escoja una de las categorias presentadas: "<<endl;
	imprimirCategoriasProductos();
	cin>>opcion;
	if(opcion==1||opcion==2||opcion==3){
		cout<<"\nSeg�n la categoria que selecciono, los productos disponibles son los siguientes:"<<endl;
		vector <string> productos=obtenerProductos(opcion);
		imprimirProductos(productos);
		cout<<"Seleccione el producto que desea comprar: ";
		cin>>opcion;
		string producto=productos[opcion-1];
		cout<<"\nA donde quiere que se le realice el envio?"<<endl;
		cout<<"1. A mi domicilio"<<endl;
		cout<<"2. Mi ubicacion actual"<<endl;
		cout<<"\n Por favor coloque el numero de la opcion q desea: "<<endl;
		cin >> opcionDireccion;
		switch(opcionDireccion){
			case 1:
				coordX=comprador.coordX;
				coordY=comprador.coordY;
				casa=true;
				break;
			case 2:
				cout << "\nPor favor digite las coordenadas de su ubicacion separadas por espacio: ";
				cin >> coordX >> coordY;
				casa=false;
				break;
			default:
				do{
					cout<<"\nPor favor, digite una opcion valida"<<endl;
					cin>>opcionDireccion;
					switch(opcionDireccion){
						case 1:
							coordX=comprador.coordX;
							coordY=comprador.coordY;
							casa=true;
							bueningreso=true;
							break;
						case 2:
							cout << "\nPor favor digite las coordenadas de su ubicacion separadas por espacio: ";
							cin >> coordX >> coordY;
							casa=false;
							bueningreso=true;
							break;
					}
				}while(bueningreso==false);
		}
		cout<<"\n Las sucursales mas cercanas en las q se vende "<<producto<<" son: "<<endl;
		vector < pair <struct tienda,float> > tiendas=buscarTiendas(producto,coordX,coordY);
		for(unsigned int i=0;(i<3 && i<tiendas.size());i++){
			cout << (i+1) << ". Nombre: " << tiendas[i].first.nombre << endl;
			cout << "Distancia: " << tiendas[i].second << endl;
			cout << setprecision(2);
			cout << fixed;
			cout << "Precio unitario: S/." << buscarPrecio(producto,tiendas[i].first.nombre) << endl;

		}
		cout<<"\nPor favor, digite el numero de la tienda a su eleccion"<<endl;
		string opcionTienda;
		cin >> opcionTienda;
		if(opcionTienda=="1" || opcionTienda=="2" || opcionTienda=="3"){
			cout << "\nPor favor digite la cantidad de unidades del producto: ";
			cin >> cantidad;
			int opcionEntero=stringaint(opcionTienda)-1;
			float precioTotal=(buscarPrecio(producto,tiendas[opcionEntero].first.nombre)*cantidad)+4;
			cout << "\nEl monto total a pagar por este pedido seria S/." << precioTotal << " (incluye tarifa por delivery de S/.4)" << endl;
			cout << "\nCon cuanto desea pagar?: S/.";
			float pagaCon;
			cin >> pagaCon;
			cout << "\nDigite 1 para confirmar el pedido o 0 para cancelar: ";
			cin >> opcion2;
			if(opcion2=="1"){
				generarPedido(producto,cantidad,precioTotal,pagaCon,casa,coordX,coordY,tiendas[stringaint(opcionTienda)-1].first,comprador);
				agregarHistorial(producto,cantidad,precioTotal,tiendas[stringaint(opcionTienda)-1].first,comprador);
				cout << "\nHa realizado su pedido con exito" << endl;
				cout << "\nPuede consultar el estado de su pedido en la opcion Ver estado de pedido" << endl;
				cout << "\nDesea realizar otro pedido? (S/N): ";
				cin >> opcion2;
				if(opcion2=="s" || "S"){
					compradorComprar(comprador);
				}else if(opcion2=="n" || "N"){
					compradorLog(intastring(comprador.numero));
				}else{
					bueningreso=false;
					while(bueningreso==false){
					cout << "\nPor favor elija una opcion valida";
					cin >> opcion2;
					if(opcion2=="s" || "S"){
					compradorComprar(comprador);
					bueningreso=true;
					break;
					}else if(opcion2=="n" || "N"){
						compradorLog(intastring(comprador.numero));
						bueningreso=true;
						break;
						}
					}
				}
			}else if(opcion2=="0"){
				compradorComprar(comprador);
			}else{
				bueningreso=false;
				while(bueningreso==false){
					cout << "\nPor favor elija una opcion valida: ";
					cin >> opcion2;
					if(opcion2=="1"){
						generarPedido(producto,cantidad,precioTotal,pagaCon,casa,coordX,coordY,tiendas[stringaint(opcionTienda)-1].first,comprador);
						agregarHistorial(producto,cantidad,precioTotal,tiendas[stringaint(opcionTienda)-1].first,comprador);
						bueningreso=true;
						cout << "\nHa realizado su pedido con exito" << endl;
						cout << "\nPuede consultar el estado de su pedido en la opcion Ver estado de pedido" << endl;
						cout << "\nDesea realizar otro pedido? (S/N): ";
						cin >> opcion;
						if(opcion2=="s" || "S"){
							compradorComprar(comprador);
						}else if(opcion2=="n" || "N"){
							compradorLog(intastring(comprador.numero));
							bueningreso=true;
						}
					}else if(opcion2=="0"){
						compradorComprar(comprador);
					}
				}
			}
		}else{
			while(true){
				cout << "\nPor favor elija una opcion valida: ";
				cin >> opcion;
			}
		}
	}else if(opcion==0){
		compradorLog(intastring(comprador.numero));
	}else{
		cout << "Por favor elija una opcion valida" << endl;
		Sleep(2000);
		compradorComprar(comprador);
	}
}
//Aqui termina compradorComprar


//INICIA VER PEDIDOS FINALIZADOS
void verPedidosFinalizados(struct comprador comprador){
	system("cls");
	unsigned int opcionPedido;
	vector <struct pedidoHistorial> pedidosFinalizados=obtenerPedidosFinalizados(comprador);
	imprimirPedidosFinalizados(pedidosFinalizados);
	cout << "\nElija el pedido con el repartidor que quiere puntuar o digite 0 para regresar: ";
	cin >> opcionPedido;
	if(opcionPedido==0){
		estadoPedido(comprador);
	}else if(opcionPedido<=pedidosFinalizados.size() && opcionPedido>0){
		float puntuacion;
		cout << "\nPor favor digite una puntuacion del 1 al 10 o digite 0 para volver: ";
		cin >> puntuacion;
		if(puntuacion>0 && puntuacion<=10){
			struct repartidor repartidor=obtenerRepartidor(pedidosFinalizados[opcionPedido-1].telefonoRepartidor);
			repartidor.ingresarPuntuacion(puntuacion);
			modificarRepartidor(repartidor);
			cambiarEstadoPuntuacionHistorialPedidos(pedidosFinalizados[opcionPedido-1]);
			cout << "La puntuacion fue registrada con exito" << endl;
			Sleep(2000);
			estadoPedido(comprador);
		}else if(puntuacion==0){
			estadoPedido(comprador);
		}else{
			cout << "Por favor ingrese una puntuacion valida";
			Sleep(2000);
			verPedidosFinalizados(comprador);
		}
	}else{
		cout << "Por favor elija una opcion valida" << endl;
		Sleep(2000);
		verPedidosFinalizados(comprador);
	}
}
//ACABA VER PEDIDOS FINALIZADOS
