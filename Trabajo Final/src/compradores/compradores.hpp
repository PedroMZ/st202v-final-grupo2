/*
 * compradores.hpp
 *
 *  Created on: 23 may. 2018
 *      Author: Pedro Marroquin
 */

#ifndef COMPRADORES_COMPRADORES_HPP_
#define COMPRADORES_COMPRADORES_HPP_


void interfazComprador();
void ingresarComprador();
void registrarComprador();
void compradorLog(string telefono);
void estadoPedido(struct comprador comprador);
void verPedidosPendientes(struct comprador comprador);
void verPedidosFinalizados(struct comprador comprador);

#endif /* COMPRADORES_COMPRADORES_HPP_ */
