#include "repartidores.hpp"
#include "../utilitarios/utilitarios.hpp"
#include "../principal/principal.hpp"
using namespace std;
void login(string usuario);

void pantallaRepartidor(){
	system("cls");
	char opcion;
	cout<<"\n\t\tBienvenido a la interfaz de Repartidores" << endl;
	cout<<"\n�Que desea hacer?"<<endl;
	cout<<"1. Ingresar"<<endl;
	cout<<"2. Registrarse"<<endl;
	cout<<"3. Regresar"<<endl;
	cout<<"0. Salir"<<endl;
	cout<<"\nPor favor elija una opcion: ";
	cin>>opcion;
	switch(opcion){
	case '1':
		ingresarRepartidor();
		break;
	case '2':
		registrarRepartidor();
		break;
	case '3':
		pantallaPrincipal();
		break;
	case '0':
		cerrarApp();
		break;
	default:
		cout<<"Por favor, elija una opcion valida"<<endl;
		Sleep(2000);
		pantallaRepartidor();
	}
}








//_-------------------------------------------------------------------------------------------------------------------------------_//
void ingresarRepartidor(){
	system("cls");
	string usuario;
	string contrasena;
	cout<<"\n\t\tBienvenido a la interfaz de identificacion de Repartidores"<<endl;
	cout<<"\nPor favor, ingrese su nombre de usuario: "<<endl;
	cin>>usuario;
	string ruta="archivos/repartidores/listaRepartidores.txt";
	ifstream archivo;
	archivo.open(ruta.c_str());
	if(archivo.is_open()){
		string linea;
		string verdadero;
		bool registrado=false;
		while(!archivo.eof() && registrado==false){
			getline(archivo,linea);
			if(linea!=""){
				if(linea.find(usuario)<200){
					stringstream ss(linea);
					int i=0;
					while(i<4){
						getline(ss,verdadero,'|');
						i++;
					}
					registrado=true;
				}
			}
		}
		archivo.close();
		if(registrado==true){
			cout<<"\nBienvenido "<<verdadero<<endl;
			cout<<"\nPor favor ingrese su password:"<<endl;
			cin>>contrasena;
			string password;
			int i=0;
			stringstream ss(linea);
			while(i<6){
				getline(ss,password,'|');
				i++;
			}
			i=0;
			while(contrasena!=password && i<3){
				if(i==2){
					cout<<"\nSu clave no es correcta, ingrese la clave correcta una vez mas: "<<endl;
				}else{
					cout<<"\nSu clave no es correcta, ingrese la clave correcta. Numero de intentos "<< (3-i)<<endl;
				}
				cin>>contrasena;
				i++;
			}
			if(contrasena!=password && i==3){
				stringstream ss(linea);
				int j=0;
				string dato,DNI,usuario,correo,contrasena;
				while(j<8){
					getline (ss,dato,'|');
						if(j==2){
							DNI=dato;
						}if(j==3){
							usuario=dato;
						}else if(j==4){
							correo==dato;
						}else if(j==5){
							contrasena=dato;
						}
						j++;
					}
				cout<<"\nClave incorrecta tres veces. Por favor, revise su correo"<<endl;
				cout<<correo<<endl;
				cout<<"\nObtendra sus credenciales en su correo";
				string rutaalterna="archivos/repartidores/recuperacion/"+correo+".txt";
				ofstream recuperador;
				recuperador.open(rutaalterna.c_str());
				if(recuperador.is_open()){
					recuperador<<"Saludos "<<usuario<<", estos son sus datos para poder iniciar sesion"<<endl;
					recuperador<<"\nNombre de usuario: "<<usuario<<endl;
					recuperador<<"Clave: "<<contrasena;
				}else{
					cout<<"El archivo no pudo abrirse"<<endl;
				}
				recuperador.close();
				Sleep(5000);
				exit(0);
			}else{
				login(usuario);
			}
		}else{
			cout<<"\nEl nombre de usuario no se encuentra registrado, por favor intente nuevamente o registrese"<<endl;
			Sleep(3000);
			ingresarRepartidor();
		}
	}else{
		cout<<"No se pudo abrir el archivo";
	}
}
void login(string usuario){
	system("cls");
	struct repartidor repartidor=obtenerRepartidor(usuario);
	cout<<"\n\t\tInterfaz de Repartidores"<<endl;
	cout<<"\nBienvenido "<<repartidor.nombre<<endl;
	if(repartidor.numpuntuaciones>0){
		cout<<"Su puntuacion actual es: "<<repartidor.puntuacion<<"[1,10]"<<endl;
		cout<<"De un total de "<<repartidor.numpuntuaciones<<" puntuaciones";
	}
	int opcion;
	cout<<"\n�Que desea hacer?"<<endl;
	cout<<"1.Tomar nuevo pedido"<<endl;
	cout<<"2.Completar pedido"<<endl;
	cout<<"0.Salir"<<endl;
	cout<<"Por favor, elija una opcion: ";
	cin>>opcion;
	switch(opcion){
	case 1: tomarPedido(repartidor);break;
	case 2: finalizarPedido(repartidor);break;
	case 0:cerrarApp();break;
	}
}
void tomarPedido(struct repartidor repartidor){
	system("cls");
	unsigned int eleccion=0;
	string opcion;
	vector <struct pedidoDisponible> pedidosDisponibles;
	pedidosDisponibles= obtenerPedidosDisponibles();
	imprimirPedidosDisponibles(pedidosDisponibles);
	cout<<"\nIngrese el numero de pedido que desea tomar o digite 0 para volver: ";
	cin>>eleccion;
	if(eleccion==0){
		login(repartidor.nombre);
	}
	if(eleccion>0 && eleccion<=pedidosDisponibles.size()){
		cout<<"\n�Desea tomar el pedido seleccionado?(S/N): ";
		cin>>opcion;
		if(opcion=="s"||opcion=="S"){
			modificarHistorial(pedidosDisponibles[eleccion-1].telefonoComprador,pedidosDisponibles[eleccion-1].horaPedido,repartidor);
			cout<<"\nPedido tomado con exito"<<endl;
			Sleep(2000);
			login(repartidor.usuario);
			}else if(opcion=="n"||opcion=="N"){
				tomarPedido(repartidor);
			}else{
				while(true){
					cout<<"\n Por favor, elija una opcion valida";
					Sleep(2000);
					tomarPedido(repartidor);
				}
			}
		}
	}
void finalizarPedido(struct repartidor repartidor){
	system("cls");
	unsigned int pedido=0;
	string opcion;
	vector <struct pedidoHistorial> pedidosTomados;
	pedidosTomados=obtenerPedidosTomados(repartidor);
	imprimirPedidosTomados(pedidosTomados);
	cout<<"\nIngrese el numero del pedido que desea finalizar o digite 0 para volver: ";
	cin>>pedido;
	if(pedido>0 && pedido<=pedidosTomados.size()){
		cout<<"\nDesea finalizar el pedido seleccionado? (ya fue entregado)(S/N): ";
		cin>>opcion;
		if(opcion=="s"||opcion=="S"){
			FinalizarPedidoHistorial(pedidosTomados[pedido-1].telefonoComprador,pedidosTomados[pedido-1].horaPedido);
			cout<<"Pedido finalizado con exito"<<endl;
			Sleep(2000);
			login(repartidor.usuario);
		}else if(opcion=="n"||opcion=="N"){
			finalizarPedido(repartidor);
		}else{
			while(true){
				cout<<"Por favor, elija una opcion valida";
				cin>>opcion;
				if(opcion=="s" || opcion=="S"){
					FinalizarPedidoHistorial(pedidosTomados[pedido-1].telefonoComprador,pedidosTomados[pedido-1].horaPedido);
					cout << "\nPedido finalizado con exito" << endl;
					Sleep(2000);
					login(repartidor.usuario);
				}else if(opcion=="n" || opcion=="N"){
					finalizarPedido(repartidor);
				}
			}
		}
	}else if(pedido==0){
		login(repartidor.usuario);
	}else{
		cout<<"Por favor, ingrese una ocpion valida"<<endl;
		Sleep(2000);
		finalizarPedido(repartidor);
	}
}
void registrarRepartidor(){
	struct repartidor r;
	system("cls");
	cout << "\n\t\tBienvenido a la interfaz de registro de repartidores\n" << endl;
	cout << "Por favor, ingrese su nombre:" << endl;
	cin.ignore();
	getline(cin, r.nombre);
	bool encontrado=buscarRepartidor(r.nombre);
	if(encontrado==true){
		do{
			cout << "\nEse nombre de repartidor ya se encuentra registrado, por favor escriba otro:" << endl;
			getline(cin, r.nombre);
			encontrado=buscarRepartidor(r.nombre);
		}while(encontrado==true);
	}
	cout <<"\nPor favor, ingrese su numero telefonico o celular:" << endl;
	cin >> r.numero;
	cout <<"\nPor favor, ingrese su DNI:" << endl;
	cin >> r.DNI;
	encontrado=buscarPalabraTienda(r.DNI);
		if(encontrado==true){
			do{
				cout << "\nEse usuario ya se encuentra en uso, por favor elija otro:" << endl;
				cin >> r.DNI;
				encontrado=buscarTienda(r.DNI);
			}while(encontrado==true);
		}
	cout << "\nPor favor escriba el usuario que desea para ingresar a la aplicacion:" << endl;
	cin >> r.usuario;
	encontrado=buscarRepartidor(r.usuario);
		if(encontrado==true){
		do{
			cout << "\nEse usuario ya se encuentra en uso, por favor elija otro:" << endl;
			cin >> r.usuario;
			encontrado=buscarRepartidor(r.usuario);
		}while(encontrado==true);
	}
	cout << "\nPor favor escriba su correo electronico: " << endl;
	cin >> r.correo;
	encontrado=buscarRepartidor(r.correo);
	if(encontrado==true){
		do{
			cout << "\nEse correo ya se encuentra registrado, por favor escriba otro:" << endl;
			cin >> r.correo;
			encontrado=buscarRepartidor(r.correo);
		}while(encontrado==true);
	}
	r.crearClave();
	r.puntuacion=0;
	r.numpuntuaciones=0;
	r.agregarRepartidor();
	r.crearArchivoConfirmacion();
	cout << "\nRepartidor registrado con exito" << endl;
	cout << "Recibira un correo de confirmacion";
	cout << "Sus datos seran revisados y se le notificara cuando su cuenta sea activada" << endl;
	Sleep(3000);
	exit(0);

}
